import { Component, OnInit } from '@angular/core';
import {ModelListItem} from '../interfaces/model-list-item';
import {ModelService} from '../services/model.service';
import {Router} from '@angular/router';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  private models: Array<ModelListItem>;
  private empty: boolean;

  constructor(private modelService: ModelService, private router: Router, private userService: UserService) {
  }

  ngOnInit() {
    this.models = [];
    setTimeout( () => this.emptyList(), 3500); // timeout = 3,5s
    this.modelService.getModels().then( r => {
      this.models = r;
      if (r.length !== 0) {
        this.empty = false;
      }
    });
  }

  getDetails(item: ModelListItem): ModelListItem {
    if (item.details == null) {
      this.modelService.getModelDetails(item.id).then(r => {
        item.details = r;
      });
    }
    return item;
  }

  emptyList(): void {
    if (this.empty !== false) {
      this.empty = true;
    }
  }

  useModel(id: number): void {
    this.router.navigate(['/model/' + id]);
  }

}
