import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {UserService} from '../services/user.service';
import {NewUser} from '../interfaces/new-user';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  usernameReg: string;
  passwordReg: string;
  usernameLog: string;
  passwordLog: string;
  email: string;
  errorReg: boolean;
  errorLog: boolean;

  constructor(public dialogRef: MatDialogRef<AccountComponent>, public userService: UserService) { }

  ngOnInit() {
    this.errorLog = false;
    this.errorReg = false;
  }

  login(): void {
    this.userService.login(this.usernameLog, this.passwordLog).subscribe(
      () => {
        this.userService.getAccountDetails();
        this.close();
      },
      () => {
        this.errorLog = true;
      }
    );
  }

  register(): void {
    const user: NewUser = ({
      email: this.email,
      username: this.usernameReg,
      password: this.passwordReg
    });
    this.userService.register(user).subscribe(
      () => {
        this.userService.login(this.usernameReg, this.passwordReg).subscribe(
          () => this.userService.getAccountDetails()
        );
        this.close();
      },
      () => {
        this.errorReg = true;
      }
    );
  }

  close(): void {
    this.dialogRef.close();
  }

}
