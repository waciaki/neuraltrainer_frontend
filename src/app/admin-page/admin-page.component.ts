import { Component, OnInit } from '@angular/core';
import {UserService} from '../services/user.service';
import {Router} from '@angular/router';
import {ModelService} from '../services/model.service';
import {ModelListItem} from '../interfaces/model-list-item';
import {ModelProbability} from '../interfaces/model-probability';
@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.scss']
})
export class AdminPageComponent implements OnInit {

  private models: Array<ModelListItem>;
  private empty: boolean;
  private loaded: boolean;
  private imgURL: any;
  private probabilities: Array<ModelProbability>;
  private image: File;
  private checking: boolean;

  constructor(private userService: UserService, private router: Router, private modelService: ModelService) { }

  ngOnInit() {
    if (!this.userService.isAdmin()) {
      this.router.navigate(['/']);
    } else {
      this.models = [];
      this.probabilities = [];
      setTimeout( () => this.emptyList(), 3500); // timeout = 3,5s
      this.modelService.getUnapprovedModels().then( r => {
        this.models = r;
        if (r.length !== 0) {
          this.empty = false;
        }
      });
    }
  }

  getDetails(item: ModelListItem): ModelListItem {
    if (item.details == null) {
      this.modelService.getModelDetails(item.id).then(r => {
        item.details = r;
      });
    }
    return item;
  }

  rejectModel(item: ModelListItem): void {
    this.models.forEach((value, index) =>  {
      if (value === item) {
        this.models = this.models.slice(index, index);
      }
    });
    this.modelService.rejectModel(item.id);
    if (this.models.length === 0) {
      this.empty = true;
    }
  }

  acceptModel(item: ModelListItem): void {
    this.models.forEach((value, index) =>  {
      if (value === item) {
        this.models = this.models.slice(index, index);
      }
    });
    this.modelService.acceptModel(item.id);
    if (this.models.length === 0) {
      this.empty = true;
    }
  }

  emptyList(): void {
    if (this.empty !== false) {
      this.empty = true;
    }
  }

  loadFile(file: File): void {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imgURL = reader.result;
    };
    this.image = file;
    this.probabilities = [];
    this.loaded = true;
  }

  checkImage(id: number): void {
    if (this.image != null) {
      this.checking = true;
      this.modelService.useModel(this.image, id).then( r => {
        if ( r != null) {
          this.checking = false;
          this.probabilities = r;
          console.log(r);
        }
      });
    }
  }

}
