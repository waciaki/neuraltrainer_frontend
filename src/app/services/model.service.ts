import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ModelListItem} from '../interfaces/model-list-item';
import {ModelDetails} from '../interfaces/model-details';
import {ModelContent} from '../interfaces/model-content';
import {NewModel} from '../interfaces/new-model';
import {ModelProbability} from '../interfaces/model-probability';

@Injectable({
  providedIn: 'root'
})
export class ModelService {

  constructor(private http: HttpClient) {}

  getModels(): Promise<ModelListItem[]> {
    return this.http.get<ModelListItem[]>('/api/models').toPromise();
  }

  getModelDetails(id: number): Promise<ModelDetails> {
    return this.http.get<ModelDetails>('/api/model/info/' + id).toPromise();
  }

  sendModel(model: File): Promise<ModelContent> {
    const formData: FormData = new FormData();
    formData.append('file', model);

    return this.http.post<ModelContent>('/api/model/get/outputs', formData).toPromise();
  }

  addModel(model: NewModel): void {
    this.http.post<NewModel>('/api/model/add', model).toPromise();
  }

  useModel(image: File, modelId: number): Promise<Array<ModelProbability>> {
    const formData: FormData = new FormData();
    formData.append('file', image);
    formData.append('modelId', modelId.toString());

    return this.http.post<Array<ModelProbability>>('/api/model/process', formData).toPromise();
  }

  downloadModel(id: number): void {
    const element = document.createElement('a');
    element.setAttribute('href', '/api/model/download/' + id);
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

  getUnapprovedModels(): Promise<ModelListItem[]> {
    return this.http.get<ModelListItem[]>('/api/models/unapproved').toPromise();
  }

  acceptModel(id: number): void {
    this.http.post('/api/model/accept/' + id, null).toPromise();
  }

  rejectModel(id: number): void {
    this.http.post('/api/model/reject/' + id, null).toPromise();
  }

}
