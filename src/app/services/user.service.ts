import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NewUser} from '../interfaces/new-user';
import {User} from '../interfaces/user';
import {Router} from '@angular/router';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private username: string;
  private superuser: boolean;
  public logged: boolean;
  private theme: Subject<string> = new Subject();
  private english: boolean;

  constructor(private http: HttpClient, private router: Router) {
    this.english = true;
    this.logged = false;
    this.superuser = false;
    this.username = null;
    this.theme.next('frontend-theme');
  }

  register(user: NewUser) {
    return this.http.put('/api/register', user);
  }

  login(username: string, password: string) {
    const formData: FormData = new FormData();
    formData.append('username', username);
    formData.append('password', password);
    return this.http.post('/api/login', formData);
  }

  logout(): void {
    this.logged = false;
    this.username = null;
    this.superuser = false;
    this.http.get('/api/logout').toPromise().then();
    this.router.navigate(['/']);
  }

  getAccountDetails(): void {
    this.http.get<User>('/api/me').toPromise().then( r => {
      this.username = r.name;
      this.superuser = r.admin;
      if (this.username !== null) {
        this.logged = true;
      }
    });
  }

  isAdmin(): boolean {
    return this.superuser;
  }

  getUsername(): string {
    return this.username;
  }

  getTheme(): Observable<string> {
    return this.theme.asObservable();
  }

  setTheme(theme: string) {
    this.theme.next(theme);
  }

  toggleLang(): void {
    this.english = !this.english;
  }

  isEnglish(): boolean {
    return this.english;
  }
}
