import { Component, OnInit } from '@angular/core';
import {ModelService} from '../services/model.service';
import {ActivatedRoute} from '@angular/router';
import {ModelDetails} from '../interfaces/model-details';
import {ModelProbability} from '../interfaces/model-probability';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-model-page',
  templateUrl: './model-page.component.html',
  styleUrls: ['./model-page.component.scss']
})
export class ModelPageComponent implements OnInit {

  private id: number;
  private details: ModelDetails;
  private image: File;
  private checking: boolean;
  private loaded: boolean;
  private imgURL: any;
  private probabilities: Array<ModelProbability>;

  constructor(private modelService: ModelService, private route: ActivatedRoute, private userService: UserService) {
    this.id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    this.loaded = false;
  }

  ngOnInit() {
    this.probabilities = [];
    this.imgURL = '../../assets/noimage.png';
    this.checking = false;
    this.loaded = false;
    this.modelService.getModelDetails(this.id).then(r => {
      this.details = r;
    });
  }

  loadFile(file: File): void {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imgURL = reader.result;
    };
    this.image = file;
    this.probabilities = [];
    this.loaded = true;
  }

  checkImage(): void {
    if (this.image != null) {
      this.checking = true;
      this.modelService.useModel(this.image, this.id).then( r => {
        if ( r != null) {
          this.checking = false;
          this.probabilities = r;
          console.log(r);
        }
      });
    }
  }

}
