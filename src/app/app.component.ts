import {Component, HostBinding, OnInit} from '@angular/core';
import {OverlayContainer} from '@angular/cdk/overlay';
import {Subscription} from 'rxjs';
import {UserService} from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  private subscription: Subscription;

  constructor(public overlayContainer: OverlayContainer, private userService: UserService) {}

  @HostBinding('class') componentCssClass;

  onSetTheme(theme) {
    this.overlayContainer.getContainerElement().classList.add(theme);
    this.componentCssClass = theme;
  }

  ngOnInit(): void {
    this.subscription = this.userService.getTheme().subscribe(theme => {
      this.onSetTheme(theme);
    });
  }

}
