import { Component, OnInit } from '@angular/core';
import {ModelService} from '../services/model.service';
import {NewModel} from '../interfaces/new-model';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../interfaces/user';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-upload-page',
  templateUrl: './upload-page.component.html',
  styleUrls: ['./upload-page.component.scss']
})
export class UploadPageComponent implements OnInit {
  uploadFormGroup: FormGroup;
  outputsFormGroup: FormGroup;
  nameFormGroup: FormGroup;
  endFormGroup: FormGroup;

  identifier: string;
  modelName: string;
  outputs: Array<string>;
  modelFile: string;
  saving: boolean;

  constructor(private modelService: ModelService, private router: Router, private formBuilder: FormBuilder, private userService: UserService) { }

  ngOnInit() {
    if (!this.userService.logged) {
      this.router.navigate(['/']);
    }
    this.uploadFormGroup = this.formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.outputsFormGroup = this.formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.nameFormGroup = this.formBuilder.group({
      thirdCtrl: ['', Validators.required]
    });
    this.endFormGroup = this.formBuilder.group(FormGroup);
    this.saving = false;
    this.outputs = [];
  }

  upload(model: File) {
    this.modelService.sendModel(model).then(r => {
      this.identifier = r.identifier;
      this.modelFile = model.name;
      this.outputs = new Array<string>(r.numberOfOutputs);
    });
  }

  getOutputsValue(): void {
    let element: any;
    for (let i = 0; i < this.outputs.length; i++) {
      element = document.getElementById('input-' + i);
      if (element.value !== '') {
        this.outputs[i] = element.value;
      }
    }
    console.log(this.outputs);
  }

  save(): void {
    const model: NewModel = ({
      name: this.modelName,
      identifier: this.identifier,
      outputs: this.outputs
    });
    this.modelService.addModel(model);
    this.saving = true;
    setTimeout(() => this.goHome(), 1500);
  }

  goHome(): void {
    this.saving = false;
    this.router.navigate(['']);
  }

}
