import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomePageComponent} from './home-page/home-page.component';
import {UploadPageComponent} from './upload-page/upload-page.component';
import {ModelPageComponent} from './model-page/model-page.component';
import {AdminPageComponent} from './admin-page/admin-page.component';

const routes: Routes = [
  {path: '', component: HomePageComponent},
  {path: 'upload', component: UploadPageComponent},
  {path: 'model/:id', component: ModelPageComponent},
  {path: 'admin', component: AdminPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
