import { Component} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog, MatIconRegistry} from '@angular/material';
import {AccountComponent} from '../account/account.component';
import {UserService} from '../services/user.service';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent {

  private theme: number;

  constructor(private router: Router, private dialog: MatDialog, private userService: UserService, private iconRegistry: MatIconRegistry, private sanitizer: DomSanitizer) {
    this.theme = 0;
    iconRegistry.addSvgIcon('theme', sanitizer.bypassSecurityTrustResourceUrl('assets/theme.svg'));
    iconRegistry.addSvgIcon('language', sanitizer.bypassSecurityTrustResourceUrl('assets/language.svg'));
  }


  goUpload() {
    this.router.navigate(['/upload']);
  }

  goHome(): void {
    this.router.navigate(['']);
  }

  goAdmin(): void {
    this.router.navigate(['/admin']);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AccountComponent, {
      width: '370px',
      height: '388px'
    });

    dialogRef.afterClosed().subscribe();
  }

  logout(): void {
    this.userService.logout();
  }

  changeTheme(): void {
    switch (this.theme) {
      case 0: {
        this.theme++;
        this.userService.setTheme('grey-theme');
        break;
      }
      case 1: {
        this.theme++;
        this.userService.setTheme('alternative');
        break;
      }
      default: {
        this.theme = 0;
        this.userService.setTheme('frontend-theme');
        break;
      }
    }
  }


}
