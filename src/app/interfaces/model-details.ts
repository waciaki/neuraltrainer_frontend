export interface ModelDetails {
  name: string;
  inputs: number;
  outputs: number;
  neurons: number;
  layers: number;
  ownerName: string;
  createdDate: Date;
  labels: Array<string>;
}
