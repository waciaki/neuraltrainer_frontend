export interface ModelContent {
  numberOfOutputs: number;
  identifier: string;
}
