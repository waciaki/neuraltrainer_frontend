import {ModelDetails} from './model-details';

export interface ModelListItem {
  id: number;
  name: string;
  details: ModelDetails;
}
