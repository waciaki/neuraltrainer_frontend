export interface NewModel {
  name: string;
  identifier: string;
  outputs: string[];
}
