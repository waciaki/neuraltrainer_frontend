export interface ModelProbability {
  label: string;
  probability: number;
}
